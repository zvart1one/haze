// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMovementState : uint8;
#ifdef HAZE_HazeCharacter_generated_h
#error "HazeCharacter.generated.h already included, missing '#pragma once' in HazeCharacter.h"
#endif
#define HAZE_HazeCharacter_generated_h

#define Haze_Source_Haze_Character_HazeCharacter_h_14_SPARSE_DATA
#define Haze_Source_Haze_Character_HazeCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define Haze_Source_Haze_Character_HazeCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define Haze_Source_Haze_Character_HazeCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHazeCharacter(); \
	friend struct Z_Construct_UClass_AHazeCharacter_Statics; \
public: \
	DECLARE_CLASS(AHazeCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Haze"), NO_API) \
	DECLARE_SERIALIZER(AHazeCharacter)


#define Haze_Source_Haze_Character_HazeCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAHazeCharacter(); \
	friend struct Z_Construct_UClass_AHazeCharacter_Statics; \
public: \
	DECLARE_CLASS(AHazeCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Haze"), NO_API) \
	DECLARE_SERIALIZER(AHazeCharacter)


#define Haze_Source_Haze_Character_HazeCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHazeCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHazeCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHazeCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHazeCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHazeCharacter(AHazeCharacter&&); \
	NO_API AHazeCharacter(const AHazeCharacter&); \
public:


#define Haze_Source_Haze_Character_HazeCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHazeCharacter(AHazeCharacter&&); \
	NO_API AHazeCharacter(const AHazeCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHazeCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHazeCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AHazeCharacter)


#define Haze_Source_Haze_Character_HazeCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(AHazeCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AHazeCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(AHazeCharacter, CursorToWorld); }


#define Haze_Source_Haze_Character_HazeCharacter_h_11_PROLOG
#define Haze_Source_Haze_Character_HazeCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Haze_Source_Haze_Character_HazeCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Haze_Source_Haze_Character_HazeCharacter_h_14_SPARSE_DATA \
	Haze_Source_Haze_Character_HazeCharacter_h_14_RPC_WRAPPERS \
	Haze_Source_Haze_Character_HazeCharacter_h_14_INCLASS \
	Haze_Source_Haze_Character_HazeCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Haze_Source_Haze_Character_HazeCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Haze_Source_Haze_Character_HazeCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Haze_Source_Haze_Character_HazeCharacter_h_14_SPARSE_DATA \
	Haze_Source_Haze_Character_HazeCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Haze_Source_Haze_Character_HazeCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Haze_Source_Haze_Character_HazeCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAZE_API UClass* StaticClass<class AHazeCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Haze_Source_Haze_Character_HazeCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
