// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Haze/Game/HazePlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHazePlayerController() {}
// Cross Module References
	HAZE_API UClass* Z_Construct_UClass_AHazePlayerController_NoRegister();
	HAZE_API UClass* Z_Construct_UClass_AHazePlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_Haze();
// End Cross Module References
	void AHazePlayerController::StaticRegisterNativesAHazePlayerController()
	{
	}
	UClass* Z_Construct_UClass_AHazePlayerController_NoRegister()
	{
		return AHazePlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AHazePlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AHazePlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_Haze,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHazePlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Game/HazePlayerController.h" },
		{ "ModuleRelativePath", "Game/HazePlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AHazePlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AHazePlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AHazePlayerController_Statics::ClassParams = {
		&AHazePlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AHazePlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AHazePlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AHazePlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AHazePlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AHazePlayerController, 1823277970);
	template<> HAZE_API UClass* StaticClass<AHazePlayerController>()
	{
		return AHazePlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AHazePlayerController(Z_Construct_UClass_AHazePlayerController, &AHazePlayerController::StaticClass, TEXT("/Script/Haze"), TEXT("AHazePlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHazePlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
