// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAZE_HazePlayerController_generated_h
#error "HazePlayerController.generated.h already included, missing '#pragma once' in HazePlayerController.h"
#endif
#define HAZE_HazePlayerController_generated_h

#define Haze_Source_Haze_Game_HazePlayerController_h_12_SPARSE_DATA
#define Haze_Source_Haze_Game_HazePlayerController_h_12_RPC_WRAPPERS
#define Haze_Source_Haze_Game_HazePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Haze_Source_Haze_Game_HazePlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHazePlayerController(); \
	friend struct Z_Construct_UClass_AHazePlayerController_Statics; \
public: \
	DECLARE_CLASS(AHazePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Haze"), NO_API) \
	DECLARE_SERIALIZER(AHazePlayerController)


#define Haze_Source_Haze_Game_HazePlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAHazePlayerController(); \
	friend struct Z_Construct_UClass_AHazePlayerController_Statics; \
public: \
	DECLARE_CLASS(AHazePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Haze"), NO_API) \
	DECLARE_SERIALIZER(AHazePlayerController)


#define Haze_Source_Haze_Game_HazePlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHazePlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHazePlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHazePlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHazePlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHazePlayerController(AHazePlayerController&&); \
	NO_API AHazePlayerController(const AHazePlayerController&); \
public:


#define Haze_Source_Haze_Game_HazePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHazePlayerController(AHazePlayerController&&); \
	NO_API AHazePlayerController(const AHazePlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHazePlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHazePlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AHazePlayerController)


#define Haze_Source_Haze_Game_HazePlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define Haze_Source_Haze_Game_HazePlayerController_h_9_PROLOG
#define Haze_Source_Haze_Game_HazePlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Haze_Source_Haze_Game_HazePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	Haze_Source_Haze_Game_HazePlayerController_h_12_SPARSE_DATA \
	Haze_Source_Haze_Game_HazePlayerController_h_12_RPC_WRAPPERS \
	Haze_Source_Haze_Game_HazePlayerController_h_12_INCLASS \
	Haze_Source_Haze_Game_HazePlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Haze_Source_Haze_Game_HazePlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Haze_Source_Haze_Game_HazePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	Haze_Source_Haze_Game_HazePlayerController_h_12_SPARSE_DATA \
	Haze_Source_Haze_Game_HazePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Haze_Source_Haze_Game_HazePlayerController_h_12_INCLASS_NO_PURE_DECLS \
	Haze_Source_Haze_Game_HazePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAZE_API UClass* StaticClass<class AHazePlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Haze_Source_Haze_Game_HazePlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
