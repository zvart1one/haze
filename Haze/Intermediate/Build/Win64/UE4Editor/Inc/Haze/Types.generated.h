// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAZE_Types_generated_h
#error "Types.generated.h already included, missing '#pragma once' in Types.h"
#endif
#define HAZE_Types_generated_h

#define Haze_Source_Haze_FuncLibrary_Types_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCharacterSpeed_Statics; \
	HAZE_API static class UScriptStruct* StaticStruct();


template<> HAZE_API UScriptStruct* StaticStruct<struct FCharacterSpeed>();

#define Haze_Source_Haze_FuncLibrary_Types_h_33_SPARSE_DATA
#define Haze_Source_Haze_FuncLibrary_Types_h_33_RPC_WRAPPERS
#define Haze_Source_Haze_FuncLibrary_Types_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Haze_Source_Haze_FuncLibrary_Types_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Haze"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define Haze_Source_Haze_FuncLibrary_Types_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Haze"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define Haze_Source_Haze_FuncLibrary_Types_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public:


#define Haze_Source_Haze_FuncLibrary_Types_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes)


#define Haze_Source_Haze_FuncLibrary_Types_h_33_PRIVATE_PROPERTY_OFFSET
#define Haze_Source_Haze_FuncLibrary_Types_h_30_PROLOG
#define Haze_Source_Haze_FuncLibrary_Types_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Haze_Source_Haze_FuncLibrary_Types_h_33_PRIVATE_PROPERTY_OFFSET \
	Haze_Source_Haze_FuncLibrary_Types_h_33_SPARSE_DATA \
	Haze_Source_Haze_FuncLibrary_Types_h_33_RPC_WRAPPERS \
	Haze_Source_Haze_FuncLibrary_Types_h_33_INCLASS \
	Haze_Source_Haze_FuncLibrary_Types_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Haze_Source_Haze_FuncLibrary_Types_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Haze_Source_Haze_FuncLibrary_Types_h_33_PRIVATE_PROPERTY_OFFSET \
	Haze_Source_Haze_FuncLibrary_Types_h_33_SPARSE_DATA \
	Haze_Source_Haze_FuncLibrary_Types_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Haze_Source_Haze_FuncLibrary_Types_h_33_INCLASS_NO_PURE_DECLS \
	Haze_Source_Haze_FuncLibrary_Types_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAZE_API UClass* StaticClass<class UTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Haze_Source_Haze_FuncLibrary_Types_h


#define FOREACH_ENUM_EMOVEMENTSTATE(op) \
	op(EMovementState::Aim_State) \
	op(EMovementState::Walk_State) \
	op(EMovementState::Run_State) 

enum class EMovementState : uint8;
template<> HAZE_API UEnum* StaticEnum<EMovementState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
