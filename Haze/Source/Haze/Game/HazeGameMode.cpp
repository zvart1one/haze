// Copyright Epic Games, Inc. All Rights Reserved.

#include "HazeGameMode.h"
#include "HazePlayerController.h"
#include "Haze/Character/HazeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHazeGameMode::AHazeGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AHazePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
