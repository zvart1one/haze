// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HazeGameMode.generated.h"

UCLASS(minimalapi)
class AHazeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHazeGameMode();
};



